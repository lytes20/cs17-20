package com.hamsoftug.dressapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.hamsoftug.dressapp.Activities.Admin;
import com.hamsoftug.dressapp.Activities.MainActivity;
import com.hamsoftug.dressapp.tools.DA;
import com.hamsoftug.dressapp.tools.Generic;
import com.hamsoftug.dressapp.tools.Models;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.hamsoftug.dressapp.tools.Constants.BASE_URL;
import static com.hamsoftug.dressapp.tools.Constants.LOGS;
import static com.hamsoftug.dressapp.tools.Constants.USER_TYPE_ADMIN;

/**
 * A login screen that offers login via email/password.
 */
public class Account extends AppCompatActivity {

    // UI references.
    private MaterialEditText mEmailView, mPasswordView, reg_full_names, reg_email, reg_password, reg_password_confirm;
    private Spinner reg_gender;
    private View mProgressView, mLoginFormView, mRegisterForm;
    private boolean is_login_form = true;
    private AppCompatTextView go_to_login, go_to_register;
    private Button reg_btn, mEmailSignInButton;
    private String _gender = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        if(Generic.getMyProfile() != null){

            if(Generic.getMyProfile().type.equals(USER_TYPE_ADMIN)){
                startActivity(new Intent(Account.this, Admin.class));
            } else {
                startActivity(new Intent(Account.this, MainActivity.class));
            }

            finish();
        }

        // Set up the login form.
        mEmailView = (MaterialEditText) findViewById(R.id.email);

        mPasswordView = (MaterialEditText) findViewById(R.id.password);

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        reg_btn = (Button) findViewById(R.id.reg_btn);

        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        mRegisterForm = findViewById(R.id.reg_form);

        go_to_login = (AppCompatTextView) findViewById(R.id.go_to_sign_in);
        go_to_register = (AppCompatTextView) findViewById(R.id.go_to_sign_up);

        reg_email = (MaterialEditText) findViewById(R.id.reg_email);
        reg_password_confirm = (MaterialEditText) findViewById(R.id.reg_password_confirm);
        reg_password = (MaterialEditText) findViewById(R.id.reg_password);
        reg_full_names = (MaterialEditText) findViewById(R.id.reg_full_names);
        reg_gender = (Spinner) findViewById(R.id.reg_gender);

        reg_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                _gender = reg_gender.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        reg_password_confirm.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.reg_btn || id == EditorInfo.IME_NULL) {
                    attemptRegister();
                    return true;
                }
                return false;
            }
        });

        reg_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegister();
            }
        });

        go_to_login.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mRegisterForm.setVisibility(View.GONE);
                mLoginFormView.setVisibility(View.VISIBLE);
                is_login_form = true;
            }
        });

        go_to_register.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                //Make views empty
                reg_email.setText(null);
                reg_full_names.setText(null);
                reg_password.setText(null);
                reg_password_confirm.setText(null);

                //Hide login form and show register form
                mRegisterForm.setVisibility(View.VISIBLE);
                mLoginFormView.setVisibility(View.GONE);
                is_login_form = false;
            }
        });

    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        is_login_form = true;

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            doLogin(password,email);

        }
    }

    private void attemptRegister() {
        is_login_form = false;

        // Reset errors.
        reg_email.setError(null);
        reg_full_names.setError(null);
        reg_password.setError(null);
        reg_password_confirm.setError(null);


        // Store values at the time of the login attempt.
        String email = reg_email.getText().toString();
        String password = reg_password.getText().toString();
        String password_2 = reg_password_confirm.getText().toString();
        String full_names = reg_full_names.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(full_names)) {
            reg_full_names.setError(getString(R.string.error_field_required));
            focusView = reg_full_names;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            reg_email.setError(getString(R.string.error_field_required));
            focusView = reg_email;
            cancel = true;
        } else if (!isEmailValid(email)) {
            reg_email.setError(getString(R.string.error_invalid_email));
            focusView = reg_email;
            cancel = true;
        }

        // Check for a valid password
        if (TextUtils.isEmpty(password)) {
            reg_password.setError(getString(R.string.error_field_required));
            focusView = reg_password;
            cancel = true;
        }

        if(!isPasswordValid(password)){
            reg_password.setError(getString(R.string.error_invalid_password));
            focusView = reg_password;
            cancel = true;
        }

        if(TextUtils.isEmpty(password_2)){
            reg_password_confirm.setError(getString(R.string.error_field_required));
            focusView = reg_password_confirm;
            cancel = true;
        }

        if(!password.equals(password_2)){
            reg_password_confirm.setError(getString(R.string.pw_not_matching));
            focusView = reg_password_confirm;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            doRegister(new Models.User(full_names,_gender,email,password));
        }
    }

    private void doRegister(Models.User user) {
        Log.e(LOGS,"Attempting register");

        final HashMap<String, String> params = new HashMap<>();
        params.put("full_names", user.full_names);
        params.put("email_address",user.email_address);
        params.put("gender", user.gender);
        params.put("password", user.password);

        StringRequest stringRequest = new StringRequest(Request.Method.POST,BASE_URL+"?todo=register", new RegListener(), new RegError())
        {
            @Override
            protected Map<String, String> getParams(){
                return params;
            }
        };

        DA.getInstance().addToRequestQueue(stringRequest,Account.this);
    }

    private boolean isEmailValid(String email) {

        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {

        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */

    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            if(is_login_form){
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);

                mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                        show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                    }
                });
            } else {
                mRegisterForm.setVisibility(show ? View.GONE : View.VISIBLE);

                mRegisterForm.animate().setDuration(shortAnimTime).alpha(
                        show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mRegisterForm.setVisibility(show ? View.GONE : View.VISIBLE);
                    }
                });
            }

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);

            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
    }

    private void doLogin(String password, String user_email){

        final HashMap<String, String> params = new HashMap<>();
        params.put("password",password);
        params.put("user_email",user_email);

        StringRequest stringRequest = new StringRequest(Request.Method.POST,BASE_URL+"?todo=login",new LoginListener(), new LoginError())
        {
            @Override
            protected Map<String, String> getParams(){

                return params;
            }
        };

        DA.getInstance().addToRequestQueue(stringRequest,getApplicationContext());

    }

    @Override
    protected void onStop() {
        super.onStop();
        DA.getInstance().cancelPendingRequests(getApplicationContext());
        DA.getInstance().cancelPendingRequests(Account.this);
    }

    private class LoginListener implements Response.Listener<String> {

        String package_name = "LoginListener";

        @Override
        public void onResponse(String response) {
            showProgress(false);

            JSONObject jsonObj = null;

            try {
                jsonObj = new JSONObject(response); //Convert login response into JSON Object
            } catch (JSONException e) {
                Log.e(package_name,e.toString());
                e.printStackTrace();
            } catch (Exception e){
                Log.e(package_name,e.toString());
            }

            try {
                JSONArray resultsArray = jsonObj.getJSONArray("result");

                for(int n=0; n<resultsArray.length(); n++){

                    JSONObject result = resultsArray.getJSONObject(n);

                    String u_number = result.getString("u_number");
                    String u_date_time = result.getString("u_date_time");
                    String u_full_names = result.getString("u_full_names");
                    String u_email = result.getString("u_email");
                    String u_password = result.getString("u_password");
                    String u_gender = result.getString("u_gender");
                    String u_type = result.getString("u_type");


                    long nn = DA.getInstance().getDb().saveUser(new Models.User(u_number,u_full_names,u_gender,u_email, u_password, u_date_time, u_type));

                    if(nn>0){

                        /*
                        *
                        if(u_type.equals(USER_TYPE_ADMIN)){
                            startActivity(new Intent(Account.this, Admin.class));
                        } else {
                            startActivity(new Intent(Account.this, MainActivity.class));
                        }
                        * */
                        startActivity(new Intent(Account.this,Account.class));
                        finish();

                    } else {
                        Generic.dialogMsg(new Generic.dialogMsg(Account.this,"Login results", "Failed to save your profile.\nPlease try again later."));
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
                Log.e(package_name,e.toString());
            }

        }
    }

    private class LoginError implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {

            showProgress(false);
            Generic.dialogMsg(new Generic.dialogMsg(Account.this,"Request a/c error",error.toString()));

        }
    }

    private class RegListener implements Response.Listener<String> {
        @Override
        public void onResponse(String response) {
            showProgress(false);
            Generic.dialogMsg(new Generic.dialogMsg(Account.this,"Registration response",response));
        }
    }

    private class RegError implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            showProgress(false);
            Generic.dialogMsg(new Generic.dialogMsg(Account.this,"Registration error",error.toString()));
        }
    }
}

