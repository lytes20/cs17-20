package com.hamsoftug.dressapp.Activities;

import android.Manifest;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.system.ErrnoException;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.hamsoftug.dressapp.Account;
import com.hamsoftug.dressapp.R;
import com.hamsoftug.dressapp.tools.DA;
import com.hamsoftug.dressapp.tools.Generic;
import com.hamsoftug.dressapp.tools.Models;
import com.hamsoftug.dressapp.tools.SpinnerAdapters;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.drakeet.materialdialog.MaterialDialog;

import static com.hamsoftug.dressapp.tools.Constants.BASE_URL;

public class AddOutFit extends AppCompatActivity {

    private Spinner out_fit_category, out_fit_shop, out_fit_gender, out_fit_skin;
    private MaterialEditText out_fit_desc, out_fit_age_range, burst, waist, hips, inseam,sleeves, shoulders, m_waist,m_inseam;
    private SpinnerAdapters.Shop shopAdapter;
    private SpinnerAdapters.Category categoryAdapter;
    private Models.Shop selected_shop;
    private Models.Category selected_category;

    private AppCompatTextView add_category;
    private AppCompatImageView out_fit_image;

    private static final int REQUEST_CODE = 200;
    private Uri mCropImageUri;
    private Bitmap bitmap = null;
    private View female_measurements, male_measurements_1, male_measurements_2;
    private String selected_gender, selected_skin;
    private String out_fit_no = Generic.getNumber();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_out_fit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        out_fit_category = (Spinner) findViewById(R.id.out_fit_category);
        out_fit_shop = (Spinner) findViewById(R.id.out_fit_shop);
        out_fit_desc = (MaterialEditText) findViewById(R.id.out_fit_desc);
        out_fit_age_range = (MaterialEditText) findViewById(R.id.out_fit_age_range);

        add_category = (AppCompatTextView) findViewById(R.id.add_category);
        out_fit_image = (AppCompatImageView) findViewById(R.id.out_fit_image);

        female_measurements = findViewById(R.id.female_measurements);
        male_measurements_1 = findViewById(R.id.male_measurements_1);
        male_measurements_2 = findViewById(R.id.male_measurements_2);

        out_fit_gender = (Spinner) findViewById(R.id.out_fit_gender);
        out_fit_skin = (Spinner) findViewById(R.id.out_fit_skin);

        shopAdapter = new SpinnerAdapters.Shop(AddOutFit.this, Generic.getShopArrayList());
        out_fit_shop.setAdapter(shopAdapter);
        out_fit_shop.setOnItemSelectedListener(new ShopSelection());

        categoryAdapter = new SpinnerAdapters.Category(AddOutFit.this,Generic.getCategoryArrayList());
        out_fit_category.setAdapter(categoryAdapter);
        out_fit_category.setOnItemSelectedListener(new CategorySelection());

        add_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AddCategory();

            }
        });

        out_fit_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(getPickImageChooserIntent(), REQUEST_CODE);
            }
        });

        out_fit_skin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selected_skin = out_fit_skin.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        out_fit_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selected_gender = out_fit_gender.getItemAtPosition(i).toString();
                toggle_measurements();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void attempt_save(){
        String desc = out_fit_desc.getText().toString();
        String age_range = out_fit_age_range.getText().toString();

        if(bitmap==null){
            Snackbar.make(out_fit_image,"Please attach picture", Snackbar.LENGTH_LONG).show();
            return;
        }

        if(TextUtils.isEmpty(desc)){
            out_fit_desc.setError(getString(R.string.error_field_required));
            out_fit_desc.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(age_range)){
            out_fit_age_range.setError(getString(R.string.error_field_required));
            out_fit_age_range.requestFocus();
            return;
        }

        if(selected_gender.equals("Female")){

            String burst_ = burst.getText().toString();
            String waist_ = waist.getText().toString();
            String hips_ = hips.getText().toString();
            String inseam_ = inseam.getText().toString();

            Models.Outfit outfit = new Models.Outfit(out_fit_no,selected_category.number,selected_shop.number,desc,selected_gender,selected_skin,age_range,burst_,waist_,hips_,inseam_);

            doSave(outfit);

        } else {

            String waist_ = m_waist.getText().toString();
            String inseam_ = m_inseam.getText().toString();
            String shoulders_ = shoulders.getText().toString();
            String sleeves_ = sleeves.getText().toString();

            Models.Outfit outfit = new Models.Outfit(out_fit_no, selected_category.number,selected_shop.number, desc,selected_gender,selected_skin, age_range, sleeves_, waist_,shoulders_,inseam_);

            doSave(outfit);

        }

    }

    private void doSave(Models.Outfit outfit){

        final HashMap<String, String> params = new HashMap<>();

        params.put("age_range",outfit.age_range);
        params.put("category", outfit.category);
        params.put("description", outfit.description);
        params.put("gender",outfit.gender);
        params.put("number", outfit.number);
        params.put("shop", outfit.shop);
        params.put("skin_color", outfit.skin);
        params.put("waist", outfit.waist);
        params.put("hips_shoulders", outfit.hips_shoulders);
        params.put("inseam", outfit.inseam);
        params.put("picture", Generic.getStringImage(bitmap));
        params.put("user",myProfile().number);
        params.put("burst_or_sleeves", outfit.burst_or_sleeves);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL+"?todo=add_out_fit", new AddListener(), new AddError())
        {

            @Override
            protected Map<String, String> getParams(){
                return params;
            }
        };

        DA.getInstance().addToRequestQueue(stringRequest, getApplicationContext());

    }

    private void toggle_measurements(){

        if(selected_gender.equals("Female")){
            female_measurements.setVisibility(View.VISIBLE);
            male_measurements_2.setVisibility(View.GONE);
            male_measurements_1.setVisibility(View.GONE);

            //
            burst = (MaterialEditText) findViewById(R.id.burst_or_sleeves);
            waist = (MaterialEditText) findViewById(R.id.waist);
            hips = (MaterialEditText) findViewById(R.id.hips_shoulders);
            inseam = (MaterialEditText) findViewById(R.id.inseam);

        } else {
            female_measurements.setVisibility(View.GONE);
            male_measurements_2.setVisibility(View.VISIBLE);
            male_measurements_1.setVisibility(View.VISIBLE);
            //
            sleeves = (MaterialEditText) findViewById(R.id.sleeves);//
            shoulders = (MaterialEditText) findViewById(R.id.shoulders);//
            m_waist = (MaterialEditText) findViewById(R.id.m_waist);//
            m_inseam = (MaterialEditText) findViewById(R.id.m_inseam);//
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.add_out_fit, menu);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                Uri resultUri = result.getUri();

                setPreview(resultUri);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                bitmap = null;
                //Others.dialog_verify(this,"Image cropping error", error.toString());
                Generic.dialogMsg(new Generic.dialogMsg(this,"Image cropping error", error.toString()));
            }
        }

        if (resultCode == RESULT_OK && requestCode== REQUEST_CODE) {
            Uri imageUri = getPickImageResultUri(data);

            boolean requirePermissions = false;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    isUriRequiresPermissions(imageUri)) {

                // request permissions and handle the result in onRequestPermissionsResult()
                requirePermissions = true;
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            }

            if (!requirePermissions) {
                doCrop(imageUri);
            }
        }

    }

    /**
     * Test if we can open the given Android URI to test if permission required error is thrown.<br>
     */
    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver =  getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (e.getCause() instanceof ErrnoException) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            doCrop(mCropImageUri);
        } else {
            Generic.dialogMsg(new Generic.dialogMsg(this,"","Required permissions are not granted"));
        }

    }

    private void doCrop(Uri filePath){

        CropImage.activity(filePath)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAllowRotation(true)
                .setRequestedSize(330,425)
                .setMinCropResultSize(330,425)
                .setGuidelinesColor(R.color.colorAccent)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .start(this);

    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new  File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    public Intent getPickImageChooserIntent() {

// Determine Uri of camera image to  save.
        Uri outputFileUri =  getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager =  getPackageManager();

// collect all camera intents
        Intent captureIntent = new  Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam =  packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new  Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

// collect all gallery intents
        Intent galleryIntent = new  Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery =  packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new  Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

// the main intent is the last in the  list, so pickup the useless one
        Intent mainIntent =  allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if  (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity"))  {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

// Create a chooser from the main  intent
        Intent chooserIntent =  Intent.createChooser(mainIntent, "Out fit picture location");

// Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,  allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    private void setPreview(Uri filePath){

        try {
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
            out_fit_image.setImageBitmap(bitmap);
        } catch (Exception e){

            Generic.dialogMsg(new Generic.dialogMsg(this,"",e.toString()));

        }
    }

    private Models.User myProfile(){

        Models.User profile = Generic.getMyProfile();

        if(profile==null){
            startActivity(new Intent(this, Account.class));
        }

        return profile;
    }

    private void AddCategory(){
        final MaterialDialog dialog = new MaterialDialog(this);
        dialog.setTitle("Add new category");

        View view = getLayoutInflater().inflate(R.layout.add_category,null);
        dialog.setContentView(view);

        final MaterialEditText category = view.findViewById(R.id.category);

        dialog.setPositiveButton("Save category", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cat_name = category.getText().toString();

                if(TextUtils.isEmpty(cat_name)){
                    category.setError(getString(R.string.error_field_required));
                    category.requestFocus();
                    return;
                }

                save_category(new Models.Category(Generic.getNumber(),cat_name,0), category);

            }
        });

        dialog.setNegativeButton("Done", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void save_category(Models.Category category, final MaterialEditText materialEditText) {

        final HashMap<String, String> params = new HashMap<>();
        params.put("name", category.name);
        params.put("number",category.number);
        params.put("user_number", myProfile().number);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL+"?todo=add_category", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Generic.dialogMsg(new Generic.dialogMsg(AddOutFit.this,"", response));

                if(response.contains("Category added")){
                    materialEditText.setText(null);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Generic.dialogMsg(new Generic.dialogMsg(AddOutFit.this,"", error.toString()));
            }
        })
        {
            @Override
            protected Map<String, String> getParams(){
                return params;
            }
        };

        DA.getInstance().addToRequestQueue(stringRequest, getApplicationContext());

    }

    @Override
    protected void onStop() {
        super.onStop();
        DA.getInstance().cancelPendingRequests(getApplicationContext());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home){
            onBackPressed();
            return true;
        }

        if(id == R.id.action_save_out_fit){
            attempt_save();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class ShopSelection implements android.widget.AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            selected_shop = Generic.getShopArrayList().get(i);
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private class CategorySelection implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            selected_category = Generic.getCategoryArrayList().get(i);
            if(selected_category.number.equals("0")){
                AddCategory();
            }

        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private class AddError implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {

            Generic.dialogMsg(new Generic.dialogMsg(AddOutFit.this,"", error.toString()));
        }
    }

    private class AddListener implements Response.Listener<String> {
        @Override
        public void onResponse(String response) {

            out_fit_no = Generic.getNumber();
            Generic.dialogMsg(new Generic.dialogMsg(AddOutFit.this,"Server response",response));
        }
    }
}
