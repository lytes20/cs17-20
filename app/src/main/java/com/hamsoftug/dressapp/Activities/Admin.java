package com.hamsoftug.dressapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.hamsoftug.dressapp.Account;
import com.hamsoftug.dressapp.R;
import com.hamsoftug.dressapp.tools.DA;
import com.hamsoftug.dressapp.tools.Generic;
import com.hamsoftug.dressapp.tools.Models;

import static com.hamsoftug.dressapp.tools.Constants.USER_TYPE_ADMIN;

public class Admin extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Models.User profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        profile = Generic.getMyProfile();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Models.User user = DA.getInstance().getDb().getUsers();

                if(user==null){
                    startActivity(new Intent(Admin.this, Account.class));
                    finish();
                } else {
                    profile = user;

                    if(user.type.equals(USER_TYPE_ADMIN)){

                        handler.postDelayed(this,1000);

                    } else {
                        startActivity(new Intent(Admin.this, MainActivity.class));
                        finish();
                    }

                }


            }
        }, 1000);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(this);

        try{

            View headerView = navigationView.getHeaderView(0);

            TextView user_names = headerView.findViewById(R.id.user_names);
            TextView user_email = headerView.findViewById(R.id.user_email);

            user_names.setText(profile.full_names);
            user_email.setText(profile.email_address);


        } catch (Exception e){

            Log.e("Pop header", e.toString());

        }

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_log_out) {
            DA.getInstance().getDb().removeUsers();
            startActivity(new Intent(this, Account.class));
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id){
            case R.id.nav_shopping_center:
                startActivity(new Intent(this, ShoppingCenter.class));
                break;

            case R.id.nav_add_outfit:
                startActivity(new Intent(this, AddOutFit.class));
                break;

            case R.id.nav_all_users:
                startActivity(new Intent(this,AllUsers.class));
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
