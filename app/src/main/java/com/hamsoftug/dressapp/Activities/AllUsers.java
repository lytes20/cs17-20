package com.hamsoftug.dressapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.hamsoftug.dressapp.Account;
import com.hamsoftug.dressapp.R;
import com.hamsoftug.dressapp.tools.DA;
import com.hamsoftug.dressapp.tools.Models;
import com.hamsoftug.dressapp.tools.UsersAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.hamsoftug.dressapp.tools.Constants.BASE_URL;
import static com.hamsoftug.dressapp.tools.Constants.USER_TYPE_ADMIN;

public class AllUsers extends AppCompatActivity {

    private ArrayList<Models.User> users = new ArrayList<>();
    private UsersAdapter adapter;
    private Models.User profile;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_users);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        adapter = new UsersAdapter(this,users);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        ListView listView = (ListView) findViewById(R.id.all_users_list);
        listView.setAdapter(adapter);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        getUsers();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                Models.User user = DA.getInstance().getDb().getUsers();

                if(user==null){
                    startActivity(new Intent(AllUsers.this, Account.class));
                    finish();
                } else {

                    if(user.type.equals(USER_TYPE_ADMIN)){

                        getUsers();

                        handler.postDelayed(this,5000);

                    } else {
                        startActivity(new Intent(AllUsers.this, Admin.class));
                        finish();
                    }

                }

            }
        }, 5000);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if(id == android.R.id.home){
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getUsers(){

        final String package_name = "get_users";

        if(users.size()<1)
        progressBar.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL+"?todo=get_users", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.GONE);

                JSONObject jsonObj = null;

                try {
                    jsonObj = new JSONObject(response); //Convert login response into JSON Object
                } catch (JSONException e) {
                    Log.e(package_name,e.toString());
                    e.printStackTrace();
                } catch (Exception e){
                    Log.e(package_name,e.toString());
                }

                try {
                    JSONArray resultsArray = jsonObj.getJSONArray("result");

                    users.clear();

                    for(int n=0; n<resultsArray.length(); n++) {

                        JSONObject result = resultsArray.getJSONObject(n);
                        String 	u_number = result.getString("u_number");
                        String u_date_time = result.getString("u_date_time");
                        String u_full_names = result.getString("u_full_names");
                        String u_email = result.getString("u_email");
                        String u_password = result.getString("u_password");
                        String u_gender = result.getString("u_gender");
                        String u_type = result.getString("u_type");

                        users.add(new Models.User(u_number,u_full_names,u_gender, u_email, u_password, u_date_time,u_type));

                    }

                    adapter.notifyDataSetChanged();

                } catch (Exception e){

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressBar.setVisibility(View.GONE);
                Log.e("get_users", error.toString());

            }
        });

        DA.getInstance().addToRequestQueue(stringRequest,this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        DA.getInstance().cancelPendingRequests(this);
    }
}
