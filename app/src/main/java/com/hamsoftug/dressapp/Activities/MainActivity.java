package com.hamsoftug.dressapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.hamsoftug.dressapp.Account;
import com.hamsoftug.dressapp.R;
import com.hamsoftug.dressapp.fragments.Categories;
import com.hamsoftug.dressapp.tools.DA;
import com.hamsoftug.dressapp.tools.Models;

import static com.hamsoftug.dressapp.tools.Constants.USER_TYPE_CASUAL;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Models.User user = DA.getInstance().getDb().getUsers();

                if(user==null){
                    startActivity(new Intent(MainActivity.this, Account.class));
                    finish();
                } else {

                    if(user.type.equals(USER_TYPE_CASUAL)){

                        handler.postDelayed(this,1000);

                    } else {
                        startActivity(new Intent(MainActivity.this, Admin.class));
                        finish();
                    }

                }


            }
        }, 1000);

        getSupportFragmentManager().beginTransaction().replace(R.id.admin_content, new Categories()).commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if(id==R.id.action_log_out){
            startActivity(new Intent(this, Account.class));
            finish();
            return true;
        }

        return true;
    }
}
