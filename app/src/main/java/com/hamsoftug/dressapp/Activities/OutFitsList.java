package com.hamsoftug.dressapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.hamsoftug.dressapp.Account;
import com.hamsoftug.dressapp.R;
import com.hamsoftug.dressapp.tools.DA;
import com.hamsoftug.dressapp.tools.Generic;
import com.hamsoftug.dressapp.tools.Models;
import com.hamsoftug.dressapp.tools.OutFitsAdapter;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.drakeet.materialdialog.MaterialDialog;

import static com.hamsoftug.dressapp.tools.Constants.BASE_URL;

public class OutFitsList extends AppCompatActivity {

    private ArrayList<Models.Outfit> outfits = new ArrayList<>();
    private ArrayList<mostNearBy> outfits_filtered = new ArrayList<>();
    private OutFitsAdapter outFitsAdapter;
    private RecyclerView out_fits_list;
    private int grid_lines = 1;
    private String package_name = "getoutfitslistener";
    private ProgressBar progressBar;
    private Models.User myProfile = DA.getInstance().getDb().getUsers();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_out_fits_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if(myProfile==null){
            startActivity(new Intent(this, Account.class));
            finish();
        }

        if(Generic.getCategoryArrayList()==null){
            onBackPressed();
        }

        setTitle(Generic.getCategory().name);

        if(Generic.isLarge()){
            grid_lines = 2;
        }

        out_fits_list = (RecyclerView) findViewById(R.id.out_fits_list);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        outFitsAdapter = new OutFitsAdapter(this,outfits);

        setupRecyclerView(out_fits_list);

        getOutFits();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getOutFits();
                handler.postDelayed(this,3000);
            }
        }, 3000);

    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new GridLayoutManager(this, grid_lines));
        recyclerView.setAdapter(outFitsAdapter);
    }

    private void getOutFits(){

        if(outfits.size()<1){
            progressBar.setVisibility(View.VISIBLE);
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL+"?todo=get_out_fits&category="+Generic.getCategory().number, new GetLister(), new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressBar.setVisibility(View.GONE);
                Log.e("GetOutFits", error.toString()+"\n"+Generic.getCategory().toString());

            }
        });

        DA.getInstance().addToRequestQueue(stringRequest,OutFitsList.this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        DA.getInstance().cancelPendingRequests(OutFitsList.this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_out_fits_list, menu);
        return true;
    }

    private void buildProperties(){
        final MaterialDialog dialog = new MaterialDialog(this);
        dialog.setTitle("Body properties");

        View view = getLayoutInflater().inflate(R.layout.body_build,null);

        dialog.setContentView(view);

        final String[] skin = new String[1];

        final Spinner skin_spinner = view.findViewById(R.id.build_skin);
        final MaterialEditText burst = view.findViewById(R.id.build_burst);
        final MaterialEditText age = view.findViewById(R.id.build_age);
        final MaterialEditText hips = view.findViewById(R.id.build_hips);
        final MaterialEditText waist = view.findViewById(R.id.build_waist);
        final MaterialEditText inseam = view.findViewById(R.id.build_inseam);

        if(myProfile.gender.equals("Male")){
            burst.setHelperText(getString(R.string.sleeves));
            burst.setHint(getString(R.string.sleeves));

            hips.setHint(getString(R.string.shoulders));
            hips.setHelperText(getString(R.string.shoulders));
        }


        skin_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                skin[0] = skin_spinner.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Models.BBuild bodyBuild = Generic.getBodyBuild();

        if(bodyBuild != null){
            burst.setText(String.valueOf(bodyBuild.burst));
            age.setText(String.valueOf(bodyBuild.age));
            hips.setText(String.valueOf(bodyBuild.hips));
            waist.setText(String.valueOf(bodyBuild.waist));
            inseam.setText(String.valueOf(bodyBuild.inseam));
        }

        dialog.setPositiveButton("Build", new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String age_ = age.getText().toString();
                String burst_ = burst.getText().toString();
                String hips_ = hips.getText().toString();
                String waist_ = waist.getText().toString();
                String inseam_ = inseam.getText().toString();

                if(TextUtils.isEmpty(age_)){
                    age.setError(getString(R.string.error_field_required));
                    age.requestFocus();
                    return;
                }

                if(TextUtils.isEmpty(burst_)){
                    burst.setError(getString(R.string.error_field_required));
                    burst.requestFocus();
                    return;
                }

                if(TextUtils.isEmpty(hips_)){
                    hips.setError(getString(R.string.error_field_required));
                    hips.requestFocus();
                    return;
                }

                if(TextUtils.isEmpty(waist_)){
                    waist.setError(getString(R.string.error_field_required));
                    waist.requestFocus();
                    return;
                }

                if(TextUtils.isEmpty(inseam_)){
                    inseam.setError(getString(R.string.error_field_required));
                    inseam.requestFocus();
                    return;
                }

                Models.BBuild new_build = new Models.BBuild(skin[0], Float.valueOf(age_), Float.valueOf(burst_), Float.valueOf(waist_), Float.valueOf(hips_), Float.valueOf(inseam_));

                Generic.setBodyBuild(new_build);

                dialog.dismiss();
            }
        });

        dialog.setNegativeButton("Not now", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Generic.setBodyBuild(null);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if(id==android.R.id.home){
            onBackPressed();
            return true;
        }

        if(id==R.id.action_build){
            buildProperties();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class GetLister implements Response.Listener<String> {
        @Override
        public void onResponse(String response) {

            progressBar.setVisibility(View.GONE);
            JSONObject jsonObj = null;

            try {
                jsonObj = new JSONObject(response); //Convert login response into JSON Object
            } catch (JSONException e) {
                Log.e(package_name,e.toString());
                e.printStackTrace();
            } catch (Exception e){
                Log.e(package_name,e.toString());
            }

            try {
                JSONArray resultsArray = jsonObj.getJSONArray("result");

                outfits.clear();
                outfits_filtered.clear();

                Models.BBuild bodyBuild = Generic.getBodyBuild();

                for(int n=0; n<resultsArray.length(); n++){

                    JSONObject result = resultsArray.getJSONObject(n);
                    String o_number = result.getString("o_number");
                    String o_category = result.getString("o_category");
                    String o_shop = result.getString("o_shop");
                    String o_desc = result.getString("o_desc");
                    String o_gender = result.getString("o_gender");
                    String o_skin = result.getString("o_skin");
                    String o_age_range = result.getString("o_age_range");

                    String burst_or_sleevse = result.getString("burst_or_sleeves");
                    String waist = result.getString("waist");
                    String hips_shoulders = result.getString("hips_shoulders");
                    String inseam = result.getString("inseam");

                    Models.Outfit outfit = new Models.Outfit(o_number, o_category,o_shop,o_desc, o_gender, o_skin,o_age_range,burst_or_sleevse,waist,hips_shoulders,inseam);

                    if(compareBody(bodyBuild,outfit)){
                        outfits.add(outfit);
                    }

                }

                checkNearBy();

                outFitsAdapter.notifyDataSetChanged();

            } catch (Exception e){
                Log.e(package_name, e.toString());
            }

        }
    }

    private void checkNearBy(){

        if(outfits.size()<1){

            //Check for 0
            for(int n=0; n<outfits_filtered.size(); n++){
                if(outfits_filtered.get(n).property_index==0){
                    outfits.add(outfits_filtered.get(n).outfit);
                }
            }

            if(outfits.size()>0){
                return;
            }

            //Check for 1
            for(int n=0; n<outfits_filtered.size(); n++){
                if(outfits_filtered.get(n).property_index==1){
                    outfits.add(outfits_filtered.get(n).outfit);
                }
            }

            if(outfits.size()>0){
                return;
            }

            //Check for 2
            for(int n=0; n<outfits_filtered.size(); n++){
                if(outfits_filtered.get(n).property_index==2){
                    outfits.add(outfits_filtered.get(n).outfit);
                }
            }

            if(outfits.size()>0){
                return;
            }

            //Check for 3
            for(int n=0; n<outfits_filtered.size(); n++){
                if(outfits_filtered.get(n).property_index==3){
                    outfits.add(outfits_filtered.get(n).outfit);
                }
            }

        }

    }

    private boolean compareBody(Models.BBuild bodyBuild, Models.Outfit outfit){

        if(bodyBuild != null){

            if(myProfile.gender.equals(outfit.gender)){

                //Check for height or inseam
                if(!outfit.inseam.equals("0") && !(bodyBuild.inseam<=0)){

                    float inseam_ = Float.valueOf(outfit.inseam);

                    //Compare heights
                    if( (inseam_ >= bodyBuild.inseam) && (inseam_ <= (bodyBuild.inseam + 0.5)) ){

                        //Add to most nearby
                        outfits_filtered.add(new mostNearBy(outfit,4));
                        //Check for age
                        if(!outfit.age_range.equals("0") && !(bodyBuild.age<=0)){

                            float age_ = Float.valueOf(outfit.age_range);

                            if( (age_ >= (bodyBuild.age - 2) ) && (age_ <= (bodyBuild.age + 2)) ){

                                //Add to most nearby
                                outfits_filtered.add(new mostNearBy(outfit,3));
                                //Check for burst or sleeves
                                if(!outfit.burst_or_sleeves.equals("0") && !(bodyBuild.burst<=0)){

                                    float burst_sleeve = Float.parseFloat(outfit.burst_or_sleeves);

                                    //Compare burst or sleeves
                                    if( (burst_sleeve >= (bodyBuild.burst - 1) ) && (burst_sleeve <= (bodyBuild.burst + 1)) ){

                                        //Add to most nearby
                                        outfits_filtered.add(new mostNearBy(outfit,2));
                                        //Check for waist
                                        if(!outfit.waist.equals("0") && !(bodyBuild.waist<=0)){

                                            float waist = Float.parseFloat(outfit.waist);

                                            //Compare waist
                                            if( (waist >= (bodyBuild.waist - 1) ) && (waist <= (bodyBuild.waist + 1)) ){

                                                //Add to most nearby
                                                outfits_filtered.add(new mostNearBy(outfit,1));
                                                //Check for hips or shoulders
                                                if(!outfit.hips_shoulders.equals("0") && !(bodyBuild.hips<=0)){

                                                    float shoulders = Float.parseFloat(outfit.hips_shoulders);

                                                    //Compare hips or shoulders
                                                    if( (shoulders >= (bodyBuild.hips - 1) ) && (shoulders <= (bodyBuild.hips + 1)) ){

                                                        //Add most nearby
                                                        outfits_filtered.add(new mostNearBy(outfit,0));
                                                        //Compare skin
                                                        if(bodyBuild.skin_color.equals(outfit.skin)){
                                                            return true;
                                                        }

                                                    }


                                                } else if( outfit.hips_shoulders.equals("0") && (bodyBuild.hips<=0)){
                                                    return true;
                                                }

                                            }


                                        } else if( outfit.waist.equals("0") && (bodyBuild.waist<=0)){
                                            return true;
                                        }

                                    }


                                } else if( outfit.burst_or_sleeves.equals("0") && (bodyBuild.burst<=0)){
                                    return true;
                                }
                            }

                        } else if( outfit.age_range.equals("0") && (bodyBuild.age<=0) ){
                            return true;
                        }


                    }

                } else if(outfit.inseam.equals("0") && (bodyBuild.inseam<=0)){
                    //Check for other properties
                    return true;
                }
            }

        } else {
            return true;
        }

        return false;
    }

    private class mostNearBy{
        public Models.Outfit outfit;
        public int property_index;

        public mostNearBy(Models.Outfit outfit, int property_index) {
            this.outfit = outfit;
            this.property_index = property_index;
        }
    }

}
