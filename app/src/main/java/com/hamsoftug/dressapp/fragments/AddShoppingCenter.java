package com.hamsoftug.dressapp.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.hamsoftug.dressapp.Account;
import com.hamsoftug.dressapp.R;
import com.hamsoftug.dressapp.tools.DA;
import com.hamsoftug.dressapp.tools.Generic;
import com.hamsoftug.dressapp.tools.Models;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.HashMap;
import java.util.Map;

import static com.hamsoftug.dressapp.tools.Constants.BASE_URL;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddShoppingCenter extends Fragment {

    private ProgressBar progressBar;
    private AppCompatButton save_btn;
    private MaterialEditText shop_name, shop_location, shop_contacts;
    private String new_number = Generic.getNumber();
    private Models.User MyProfile = Generic.getMyProfile();

    public AddShoppingCenter() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_shopping_center, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        //super.onViewCreated(view, savedInstanceState);

        if(MyProfile==null){
            startActivity(new Intent(getContext(), Account.class));
            getActivity().finish();
        }

        shop_contacts = view.findViewById(R.id.shop_phone);
        shop_location = view.findViewById(R.id.shop_location);
        shop_name = view.findViewById(R.id.shop_name);
        save_btn = view.findViewById(R.id.shop_save_btn);
        progressBar = view.findViewById(R.id.shop_progress);

        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attempt_save();
            }
        });

    }

    private void clearFields(){
        shop_contacts.setText(null);
        shop_location.setText(null);
        shop_name.setText(null);
        new_number = Generic.getNumber();
    }

    private void attempt_save() {
        String name = shop_name.getText().toString();
        String location = shop_location.getText().toString();
        String contacts = shop_contacts.getText().toString();

        if(TextUtils.isEmpty(name)){
            shop_name.setError(getString(R.string.error_field_required));
            shop_name.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(location)){
            shop_location.setError(getString(R.string.error_field_required));
            shop_location.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(contacts)){
            shop_contacts.setError(getString(R.string.error_field_required));
            shop_contacts.requestFocus();
            return;
        }

        doSave(new Models.Shop(new_number,name,location,contacts));

    }

    private void doSave(Models.Shop shop) {

        progressBar.setVisibility(View.VISIBLE);

        final HashMap<String, String> params = new HashMap<>();
        params.put("contacts",shop.contacts);
        params.put("location", shop.location);
        params.put("name", shop.name);
        params.put("number", shop.number);
        params.put("user_number", MyProfile.number);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL+"?todo=add_shop", new SaveListener(), new SaveError())
        {

            @Override
            protected Map<String, String> getParams(){
                return params;
            }
        };

        DA.getInstance().addToRequestQueue(stringRequest,getContext());

    }

    @Override
    public void onStop() {
        super.onStop();
        DA.getInstance().cancelPendingRequests(getContext());
    }

    private class SaveListener implements Response.Listener<String> {
        @Override
        public void onResponse(String response) {

            progressBar.setVisibility(View.GONE);
            Generic.dialogMsg(new Generic.dialogMsg(getContext(),"Save response", response));

            if(response.contains("added successfully")){
                clearFields();
            }

        }
    }

    private class SaveError implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {

            progressBar.setVisibility(View.GONE);
            Generic.dialogMsg(new Generic.dialogMsg(getContext(),"Saving error", error.toString()));

        }
    }
}
