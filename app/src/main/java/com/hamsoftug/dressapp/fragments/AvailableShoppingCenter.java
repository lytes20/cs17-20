package com.hamsoftug.dressapp.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.hamsoftug.dressapp.Account;
import com.hamsoftug.dressapp.R;
import com.hamsoftug.dressapp.tools.DA;
import com.hamsoftug.dressapp.tools.Generic;
import com.hamsoftug.dressapp.tools.Models;
import com.hamsoftug.dressapp.tools.ShoppingCenterAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class AvailableShoppingCenter extends Fragment {

    private ListView shopping_center_list;
    private ShoppingCenterAdapter adapter;
    private Models.User profile;
    private String package_name = "AvShCenter";

    public AvailableShoppingCenter() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_available_shopping_center, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        //super.onViewCreated(view, savedInstanceState);

        profile = Generic.getMyProfile();

        if(profile==null){
            startActivity(new Intent(getContext(), Account.class));
            getActivity().finish();
            return;
        }

        shopping_center_list = view.findViewById(R.id.shopping_center_list);
        adapter = new ShoppingCenterAdapter(getContext(),Generic.getShopArrayList());
        shopping_center_list.setAdapter(adapter);

        check();

    }

    private void check(){
        final Handler handler = new Handler();
        final int delay = 2000;
        Generic.getShopArrayList();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                //get_shopping_centers();
                Generic.getShopArrayList();

                handler.postDelayed(this,delay);

            }
        }, delay);

    }

    @Override
    public void onStop() {
        super.onStop();
        DA.getInstance().cancelPendingRequests(getContext());
    }

}
