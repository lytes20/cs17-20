package com.hamsoftug.dressapp.fragments;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.hamsoftug.dressapp.R;
import com.hamsoftug.dressapp.tools.CategoryAdapter;
import com.hamsoftug.dressapp.tools.Generic;

/**
 * A simple {@link Fragment} subclass.
 */
public class Categories extends Fragment {

    private RecyclerView category_list;
    private CategoryAdapter adapter;
    private int grid_lines = 2;
    private View larger_screen;
    private ProgressBar progressBar;


    public Categories() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_categories, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        larger_screen = view.findViewById(R.id.category_list_larger);
        progressBar = view.findViewById(R.id.progressBar);

        if(larger_screen != null){
            grid_lines = 3;
            Generic.setIsLarge(true);
            category_list = view.findViewById(R.id.category_list_larger);
        } else {
            category_list = view.findViewById(R.id.category_list);
        }

        adapter = new CategoryAdapter(getContext(),Generic.getCategoryArrayList());

        assert category_list != null;
        setupRecyclerView(category_list);

        final Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if(Generic.getCategoryArrayList().size()<1){
                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.GONE);
                }

                adapter.notifyDataSetChanged();

                handler.postDelayed(this,2000);
            }
        }, 1000);


    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), grid_lines));
        recyclerView.setAdapter(adapter);
    }


}
