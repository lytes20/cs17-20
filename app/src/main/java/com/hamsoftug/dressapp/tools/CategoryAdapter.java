package com.hamsoftug.dressapp.tools;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hamsoftug.dressapp.Activities.OutFitsList;
import com.hamsoftug.dressapp.R;

import java.util.ArrayList;

/**
 * Created by USER on 6/28/2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private final ArrayList<Models.Category> mValues;
    private final Context context;

    public CategoryAdapter(Context context,ArrayList<Models.Category> items) {
        mValues = items;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

       /* View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_list_content, parent, false); */

        View view = LayoutInflater.from(context)
                .inflate(R.layout.home_list_content, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        final Models.Category category = mValues.get(position);
        holder.mCategoryName.setText(category.name);

        if(category.count_items>0){
            holder.mCountText.setText("Outfits: "+category.count_items);
        } else {
            holder.mCountText.setText("No outfits added");
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Snackbar.make(v,"Coming soon", Snackbar.LENGTH_LONG).show();
                Generic.setCategory(category);
                Intent intent = new Intent(context, OutFitsList.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public AppCompatTextView mCategoryName, mCountText;
        public Models.Category mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mCategoryName = view.findViewById(R.id.list_category_name);
            mCountText = view.findViewById(R.id.list_category_item_count);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mCategoryName.getText().toString() + "'";
        }
    }
}

