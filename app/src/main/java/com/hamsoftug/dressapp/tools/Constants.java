package com.hamsoftug.dressapp.tools;

/**
 * Created by USER on 6/24/2017.
 */

public class Constants {
    public static final String VOLLEY_PATTERNS_TAG = "VolleyRequest";

    public static final String BASE_URL = "http://10.0.2.2:8888/dressingapp/";

    // public static final String BASE_URL = "http://10.16.5.104/dressingapp/";

    public static final String USER_TYPE_ADMIN = "Admin";
    public static final String USER_TYPE_CASUAL = "Casual";
    public static final String LOGS = Thread.currentThread().getStackTrace()[0].getMethodName();
}
