package com.hamsoftug.dressapp.tools;

import android.app.Application;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.hamsoftug.dressapp.tools.Constants.BASE_URL;

/**
 * Created by USER on 6/24/2017.
 */

public class DA extends Application {

    private static DA Instance;
    private Db db = null;
    Random random = null;
    String package_name = "get_users";

    /**
     * Global request queue for Volley
     */
    private RequestQueue mRequestQueue;

    public static synchronized DA getInstance() {
        return Instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Instance = this;

        get_shopping_centers();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                Models.User user = getDb().getUsers();

                if(user != null){
                    get_shopping_centers();
                    syncProfile(user);
                    handler.postDelayed(this,5000);
                }

            }
        }, 5000);
    }



    public Db getDb() {

        if(db==null){
            db = new Db(getInstance());
        }

        return db;
    }

    public Random getRandom() {

        if(random==null){
            random = new Random();
        }

        return random;
    }

    private void get_shopping_centers() {

       Models.User profile = getDb().getUsers();

        if(profile==null){
            return;
        }

        get_categories();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,BASE_URL+"?todo=get_shopping_list&user="+profile.number, new ListListener(), new ListError());

        DA.getInstance().addToRequestQueue(stringRequest);
    }

    private void get_categories(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL+"?todo=get_categories", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                JSONObject jsonObj = null;

                try {
                    jsonObj = new JSONObject(response); //Convert login response into JSON Object
                } catch (JSONException e) {
                    Log.e(package_name,e.toString());
                    e.printStackTrace();
                } catch (Exception e){
                    Log.e(package_name,e.toString());
                }

                try {
                    JSONArray resultsArray = jsonObj.getJSONArray("result");

                    Generic.getCategoryArrayList().clear();

                    for(int n=0; n<resultsArray.length(); n++){
                        JSONObject jsonObject = resultsArray.getJSONObject(n);

                        String c_number = jsonObject.getString("c_number");
                        String c_name = jsonObject.getString("c_name");
                        int x= 0;
                        String item_count = jsonObject.getString("item_count");

                        try{

                            x = Integer.parseInt(item_count);

                        } catch (Exception e){
                            e.printStackTrace();
                        }


                        Generic.getCategoryArrayList().add(new Models.Category(c_number,c_name,x));

                    }

                } catch (Exception e){

                    Log.e("JSON", e.toString());

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("get_category", error.toString());

            }
        });

        DA.getInstance().addToRequestQueue(stringRequest);

    }

    private class ListListener implements Response.Listener<String> {
        @Override
        public void onResponse(String response) {

            JSONObject jsonObj = null;

            try {
                jsonObj = new JSONObject(response); //Convert login response into JSON Object
            } catch (JSONException e) {
                Log.e(package_name,e.toString());
                e.printStackTrace();
            } catch (Exception e){
                Log.e(package_name,e.toString());
            }

            try {
                JSONArray resultsArray = jsonObj.getJSONArray("result");

                Generic.getShopArrayList().clear();

                for(int n=0; n<resultsArray.length(); n++){
                    JSONObject result = resultsArray.getJSONObject(n);
                    String s_name = result.getString("s_name");
                    String s_location = result.getString("s_location");
                    String s_contacts = result.getString("s_contacts");
                    String s_number = result.getString("s_number");

                    Generic.getShopArrayList().add(new Models.Shop(s_number,s_name,s_location,s_contacts));
                }

            } catch (Exception e){
                Log.e(package_name, e.toString());
            }

        }
    }

    private class ListError implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.e("Error", error.toString());
        }
    }

    private void syncProfile(Models.User user){

        package_name = "syncProfile";

        final HashMap<String, String> params = new HashMap<>();
        params.put("number",user.number);
        params.put("token",user.password);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL+"?todo=get_profile", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                JSONObject jsonObj = null;

                try {
                    jsonObj = new JSONObject(response); //Convert login response into JSON Object
                } catch (JSONException e) {
                    Log.e(package_name,e.toString());
                    e.printStackTrace();
                } catch (Exception e){
                    Log.e(package_name,e.toString());
                }

                try {
                    JSONArray resultsArray = jsonObj.getJSONArray("result");

                    if(resultsArray.length()<1){
                        Toast.makeText(getInstance(), "Can't find your profile\nPlease re-login.", Toast.LENGTH_LONG).show();
                    }

                    DA.getInstance().getDb().removeUsers();

                    for(int n=0; n<resultsArray.length(); n++){

                        JSONObject result = resultsArray.getJSONObject(n);

                        String u_number = result.getString("u_number");
                        String u_date_time = result.getString("u_date_time");
                        String u_full_names = result.getString("u_full_names");
                        String u_email = result.getString("u_email");
                        String u_password = result.getString("u_password");
                        String u_gender = result.getString("u_gender");
                        String u_type = result.getString("u_type");


                        long nn = DA.getInstance().getDb().saveUser(new Models.User(u_number,u_full_names,u_gender,u_email, u_password, u_date_time, u_type));

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(package_name,e.toString());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams(){
                return params;
            }
        };

        getInstance().addToRequestQueue(stringRequest);
    }

    /**
     * @return The Volley Request queue, the queue will be created if it is null
     */
    private RequestQueue getRequestQueue() {
        // lazy initialize the request queue, the queue instance will be
        // created when it is accessed for the first time
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getInstance());
        }

        //mRequestQueue.getCache().clear();

        return mRequestQueue;
    }

    /**
     * Adds the specified request to the global queue, if tag is specified
     * then it is used else Default VOLLEY_PATTERNS_TAG is used.
     *
     * @param req
     * @param tag
     */
    public <T> void addToRequestQueue(Request<T> req, Object tag) {
        // set the default tag if tag is null
        req.setTag(tag==null ? Constants.VOLLEY_PATTERNS_TAG : tag);

        req.setRetryPolicy(getInstance().retryPolicy(20000));

        VolleyLog.d("Adding request to queue: %s", req.getUrl());

        getRequestQueue().add(req);
    }


    public RetryPolicy retryPolicy(int socketTimeout_in_ms){

        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout_in_ms,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        return policy;
    }

    public RetryPolicy retryPolicy(int socketTimeout_in_ms, int max_retry){

        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout_in_ms,
                max_retry,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        return policy;
    }

    /**
     * Adds the specified request to the global queue using the Default VOLLEY_PATTERNS_TAG.
     *
     * @param req
     */

    public <T> void addToRequestQueue(Request<T> req) {

        req.setRetryPolicy(getInstance().retryPolicy(20000));

        // set the default tag if tag is empty
        req.setTag(Constants.VOLLEY_PATTERNS_TAG);

        getRequestQueue().add(req);
    }

    /**
     * Cancels all pending requests by the specified VOLLEY_PATTERNS_TAG, it is important
     * to specify a VOLLEY_PATTERNS_TAG so that the pending/ongoing requests can be cancelled.
     *
     * @param tag
     */
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

}
