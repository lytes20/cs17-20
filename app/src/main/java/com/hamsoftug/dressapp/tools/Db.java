package com.hamsoftug.dressapp.tools;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by USER on 6/25/2017.
 */

public class Db extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "dressapp_db";

    public static final int DATABASE_VERSION = 1;

    // tables

    private static final String TABLE_USERS = "t_users";

    /**
     * @param TABLE_USERS fields
     */

    private static final String U_ID = "u_id";
    private static final String U_NUMBER = "U_number";
    private static final String U_NAMES = "u_name";
    private static final String U_EMAIL = "u_email";
    private static final String U_TYPE = "u_type";
    private static final String U_PASSWORD = "u_password";
    private static final String U_DATE = "u_date";
    private static final String U_GENDER = "u_gender";

    private static final String CREATE_TABLE_USERS = "CREATE TABLE "
            + TABLE_USERS + "(" + U_ID + " INTEGER PRIMARY KEY,"
            + U_DATE + " TEXT,"
            + U_EMAIL + " TEXT,"
            + U_GENDER + " TEXT,"
            + U_PASSWORD + " TEXT,"
            + U_NAMES + " TEXT,"
            + U_NUMBER + " TEXT,"
            + U_TYPE + " TEXT"
            + ")";
    private static final String LOG = "sqliteDb";

    public Db(Context context) {
        super(context,DATABASE_NAME, null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        try {
            sqLiteDatabase.execSQL(CREATE_TABLE_USERS);
        } catch (Exception e){
            Log.e(TABLE_USERS,e.toString());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        onCreate(sqLiteDatabase);

    }

    public long saveUser(Models.User userNames) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(U_DATE, userNames.date);
        values.put(U_EMAIL, userNames.email_address);
        values.put(U_GENDER, userNames.gender);
        values.put(U_NAMES, userNames.full_names);
        values.put(U_NUMBER, userNames.number);
        values.put(U_PASSWORD, userNames.password);
        values.put(U_TYPE, userNames.type);

        // insert row
        long todo_id = db.insert(TABLE_USERS, null, values);
        Log.e(LOG, "Inserting into " + TABLE_USERS);

        return todo_id;
    }

    public Models.User getUsers() {

        Models.User userNames = null;

        String selectQuery = "SELECT  * FROM " + TABLE_USERS + "";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {

                String date  = c.getString(c.getColumnIndex(U_DATE));
                String email = c.getString(c.getColumnIndex(U_EMAIL));
                String gender = c.getString(c.getColumnIndex(U_GENDER));
                String pw = c.getString(c.getColumnIndex(U_PASSWORD));
                String names = c.getString(c.getColumnIndex(U_NAMES));
                String type = c.getString(c.getColumnIndex(U_TYPE));
                String number = c.getString(c.getColumnIndex(U_NUMBER));

                userNames = new Models.User(number,names,gender,email,pw,date,type);

            } while (c.moveToNext());
        }

        return userNames;
    }

    public void removeUsers()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE from "+ TABLE_USERS);
        db.execSQL("VACUUM");
        db.close();
    }
}
