package com.hamsoftug.dressapp.tools;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Base64;
import android.view.View;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by USER on 6/24/2017.
 */

public class Generic {

    private static Models.User myProfile;
    private static ArrayList<Models.Shop> shopArrayList = new ArrayList<>();
    private static ArrayList<Models.Category> categoryArrayList = new ArrayList<>();
    private static Models.Category category;
    private static boolean IsLarge = false;
    private static Models.BBuild bodyBuild;
    private static ArrayList<Models.Outfit> combination = new ArrayList<>();

    public static ArrayList<Models.Outfit> getCombination() {
        return combination;
    }

    public static void setCombination(ArrayList<Models.Outfit> combination) {
        Generic.combination = combination;
    }

    public static Models.BBuild getBodyBuild() {
        return bodyBuild;
    }

    public static void setBodyBuild(Models.BBuild bodyBuild) {
        Generic.bodyBuild = bodyBuild;
    }

    public static boolean isLarge() {
        return IsLarge;
    }

    public static void setIsLarge(boolean isLarge) {
        IsLarge = isLarge;
    }

    public static Models.Category getCategory() {
        return category;
    }

    public static void setCategory(Models.Category category) {
        Generic.category = category;
    }

    public static Models.User getMyProfile() {

        myProfile = DA.getInstance().getDb().getUsers();

        return myProfile;
    }

    public static ArrayList<Models.Category> getCategoryArrayList() {

        return categoryArrayList;
    }

    public static String getStringImage(Bitmap bmp){

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);

        return encodedImage;
    }

    public static ArrayList<Models.Shop> getShopArrayList() {
        return shopArrayList;
    }

    public static class dialogMsg{
        Context context;
        String title, msg;

        public dialogMsg(Context context, String title, String msg) {
            this.context = context;
            this.title = title;
            this.msg = msg;
        }
    }

    public static void dialogMsg(dialogMsg dialogMsg){

        final MaterialDialog materialDialog = new MaterialDialog(dialogMsg.context);

        materialDialog.setTitle(dialogMsg.title);
        materialDialog.setMessage(dialogMsg.msg);

        materialDialog.setPositiveButton("Got it", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                materialDialog.dismiss();
            }
        });

        materialDialog.show();

    }

    public static String getNumber(){

        int aStart = 100000, aEnd = 999999;

        if (aStart > aEnd) {
            throw new IllegalArgumentException("Start cannot exceed End.");
        }
        //get the range, casting to long to avoid overflow problems
        long range = (long)aEnd - (long)aStart + 1;

        long fraction = (long)(range * DA.getInstance().getRandom().nextDouble());
        int randomNumber =  (int)(fraction + aStart);
        return String.valueOf(randomNumber);
    }

}
