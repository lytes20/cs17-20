package com.hamsoftug.dressapp.tools;

/**
 * Created by USER on 6/24/2017.
 */

public class Models {
    public static class User{
        public String number, full_names, gender, email_address, password, date, type;

        public User(String number, String full_names, String gender, String email_address, String password, String date, String type) {
            this.number = number;
            this.full_names = full_names;
            this.gender = gender;
            this.email_address = email_address;
            this.password = password;
            this.date = date;
            this.type = type;
        }

        public User(String full_names, String gender, String email_address, String password) {
            this.full_names = full_names;
            this.gender = gender;
            this.email_address = email_address;
            this.password = password;
        }

    }

    public static class Shop{
        public String number, name, location, contacts;

        public Shop(String number, String name, String location, String contacts) {
            this.number = number;
            this.name = name;
            this.location = location;
            this.contacts = contacts;
        }
    }

    public static class Category{
        public String number, name;
        public int count_items;

        public Category(String number, String name, int count_items) {
            this.number = number;
            this.name = name;
            this.count_items = count_items;
        }

        @Override
        public String toString() {
            return number+"|"+name+"|"+count_items;
        }
    }

    public static class Outfit{

        public String number,category,shop,description,gender,skin,age_range;
        public String burst_or_sleeves,waist, hips_shoulders,inseam;

        public Outfit(String number, String category, String shop, String description, String gender, String skin, String age_range, String burst_or_sleeves, String waist, String hips_shoulders, String inseam) {
            this.number = number;
            this.category = category;
            this.shop = shop;
            this.description = description;
            this.gender = gender;
            this.skin = skin;
            this.age_range = age_range;
            this.burst_or_sleeves = burst_or_sleeves;
            this.waist = waist;
            this.hips_shoulders = hips_shoulders;
            this.inseam = inseam;
        }
    }

    public static class BBuild{
        public String skin_color;
        public float age, burst, waist, hips, inseam;

        public BBuild(String skin_color, float age, float burst, float waist, float hips, float inseam) {
            this.skin_color = skin_color;
            this.age = age;
            this.burst = burst;
            this.waist = waist;
            this.hips = hips;
            this.inseam = inseam;
        }
    }
}
