package com.hamsoftug.dressapp.tools;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.hamsoftug.dressapp.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import me.drakeet.materialdialog.MaterialDialog;

import static com.hamsoftug.dressapp.tools.Constants.BASE_URL;

/**
 * Created by USER on 6/29/2017.
 */
public class OutFitsAdapter extends RecyclerView.Adapter<OutFitsAdapter.ViewHolder> {

    private final ArrayList<Models.Outfit> mValues;
    private final Context context;

    public OutFitsAdapter(Context context,ArrayList<Models.Outfit> items) {
        mValues = items;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

       /* View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_list_content, parent, false); */

        View view = LayoutInflater.from(context)
                .inflate(R.layout.single_outfit, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        final Models.Outfit outfit = mValues.get(position);
        holder.description.setText(outfit.description);

        if(outfit.gender.equals("Female")){
            holder.single_burst_sleeves_text.setText("Burst");
            holder.single_hips_shoulders_text.setText("Hips");
        } else {
            holder.single_burst_sleeves_text.setText("Sleeves");
            holder.single_hips_shoulders_text.setText("Shoulders");
        }

        holder.single_inseam.setText(outfit.inseam.equals("0")? "N/A":outfit.inseam);
        holder.single_burst_sleeves.setText(outfit.burst_or_sleeves.equals("0")? "N/A":outfit.burst_or_sleeves);
        holder.single_hips_shoulders.setText(outfit.hips_shoulders.equals("0")? "N/A":outfit.hips_shoulders);
        holder.single_waist.setText(outfit.waist.equals("0")? "N/A":outfit.waist);

        holder.single_out_fit_recommendation_text.setText("Good for "+outfit.skin+" skinned "+outfit.gender+"s of age range "+outfit.age_range);

        Picasso.with(context)
                .load(BASE_URL+"out_fits/"+outfit.number+".png")
                .error(R.mipmap.ic_launcher_round)
                .into(new MyTarget(holder.picture, holder.single_out_fit_img_progress));

        holder.picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getShopDetails(outfit,v);

            }
        });

        holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                //holder.mView.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
                //holder.mView.setEnabled(false);
                //Generic.getCombination().add(outfit);

                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        AppCompatTextView description, single_burst_sleeves_text, single_burst_sleeves, single_hips_shoulders_text, single_hips_shoulders, single_waist, single_inseam, single_out_fit_recommendation_text;
        AppCompatImageView picture;
        Models.Outfit mItem;
        ProgressBar single_out_fit_img_progress;

        ViewHolder(View view) {
            super(view);
            mView = view;
            description = view.findViewById(R.id.single_out_fit_description);
            picture = view.findViewById(R.id.single_outfit_picture);
            single_out_fit_img_progress = view.findViewById(R.id.single_out_fit_img_progress);

            single_burst_sleeves_text = view.findViewById(R.id.single_burst_sleeves_text);
            single_burst_sleeves = view.findViewById(R.id.single_burst_sleeves);
            single_hips_shoulders_text = view.findViewById(R.id.single_hips_shoulders_text);
            single_hips_shoulders = view.findViewById(R.id.single_hips_shoulders);
            single_waist = view.findViewById(R.id.single_waist);
            single_inseam = view.findViewById(R.id.single_inseam);

            single_out_fit_recommendation_text = view.findViewById(R.id.single_out_fit_recommendation_text);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + description.getText().toString() + "'";
        }
    }

    private void getShopDetails(final Models.Outfit outfit, final View v){

        final String get_shop = "GetShopDetails";

        Snackbar.make(v,"Getting more details", Toast.LENGTH_LONG).show();

        final HashMap<String, String> params = new HashMap<>();
        params.put("shop", outfit.shop);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL+"?todo=get_shop", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                JSONObject jsonObj = null;
                Snackbar.make(v,"More details", Toast.LENGTH_LONG).show();

                try {
                    jsonObj = new JSONObject(response); //Convert login response into JSON Object
                } catch (JSONException e) {
                    Log.e(get_shop,e.toString());
                    e.printStackTrace();
                } catch (Exception e){
                    Log.e(get_shop,e.toString());
                }

                try {
                    JSONArray resultsArray = jsonObj.getJSONArray("result");

                    for (int n = 0; n < resultsArray.length(); n++) {

                        JSONObject result = resultsArray.getJSONObject(n);

                        String s_name = result.getString("s_name");
                        String s_location = result.getString("s_location");
                        String s_contacts = result.getString("s_contacts");
                        String s_number = result.getString("s_number");

                        shopDetails(new Models.Shop(s_number,s_name,s_location,s_contacts), outfit);


                    }

                } catch (Exception e){
                    Log.e(get_shop, e.toString());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar.make(v,"Error try again.", Snackbar.LENGTH_LONG).setAction("Try now", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getShopDetails(outfit,v);
                    }
                }).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams(){
                return params;
            }
        };

        DA.getInstance().addToRequestQueue(stringRequest,this);

    }

    private void shopDetails(Models.Shop shop, Models.Outfit outfit){

        final MaterialDialog dialog = new MaterialDialog(context);
        dialog.setTitle("Get this outfit from:");

        View view = LayoutInflater.from(context).inflate(R.layout.shop_details, null);
        dialog.setContentView(view);

        AppCompatTextView name = view.findViewById(R.id.details_shop_name);
        AppCompatTextView loc = view.findViewById(R.id.details_shop_location);
        AppCompatTextView contacts = view.findViewById(R.id.details_shop_contacts);

        name.setText(shop.name);
        loc.setText(shop.location);
        contacts.setText(shop.contacts);

        AppCompatTextView single_burst_sleeves_text = view.findViewById(R.id.single_burst_sleeves_text);
        AppCompatTextView single_burst_sleeves = view.findViewById(R.id.single_burst_sleeves);
        AppCompatTextView single_hips_shoulders_text = view.findViewById(R.id.single_hips_shoulders_text);
        AppCompatTextView single_hips_shoulders = view.findViewById(R.id.single_hips_shoulders);
        AppCompatTextView single_waist = view.findViewById(R.id.single_waist);
        AppCompatTextView single_inseam = view.findViewById(R.id.single_inseam);

        if(outfit.gender.equals("Female")){
            single_burst_sleeves_text.setText("Burst");
            single_hips_shoulders_text.setText("Hips");
        } else {
            single_burst_sleeves_text.setText("Sleeves");
            single_hips_shoulders_text.setText("Shoulders");
        }

        single_inseam.setText(outfit.inseam.equals("0")? "N/A":outfit.inseam);
        single_burst_sleeves.setText(outfit.burst_or_sleeves.equals("0")? "N/A":outfit.burst_or_sleeves);
        single_hips_shoulders.setText(outfit.hips_shoulders.equals("0")? "N/A":outfit.hips_shoulders);
        single_waist.setText(outfit.waist.equals("0")? "N/A":outfit.waist);

        dialog.setPositiveButton("Got it", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private class MyTarget implements Target {
        private AppCompatImageView picture;
        private ProgressBar single_out_fit_img_progress;

        public MyTarget(AppCompatImageView picture, ProgressBar single_out_fit_img_progress) {
            this.picture = picture;
            this.single_out_fit_img_progress = single_out_fit_img_progress;
        }

        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            picture.setImageBitmap(bitmap);
            single_out_fit_img_progress.setVisibility(View.GONE);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            single_out_fit_img_progress.setVisibility(View.GONE);
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            single_out_fit_img_progress.setVisibility(View.VISIBLE);

        }
    }
}

