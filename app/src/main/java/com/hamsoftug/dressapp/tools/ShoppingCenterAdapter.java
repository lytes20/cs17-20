package com.hamsoftug.dressapp.tools;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.hamsoftug.dressapp.R;

import java.util.ArrayList;

/**
 * Created by USER on 6/25/2017.
 */

public class ShoppingCenterAdapter extends BaseAdapter{

    private ArrayList<Models.Shop> shopArrayList;
    private LayoutInflater inflater;

    public ShoppingCenterAdapter(Context context, ArrayList<Models.Shop> shopArrayList) {
        this.shopArrayList = shopArrayList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return shopArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return shopArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        Models.Shop mdTransactions = shopArrayList.get(i);

        ViewDetailsHolder viewHolder;

        View singleView = convertView;

        if(singleView == null) {
            singleView = inflater.inflate(R.layout.single_shop, null, true);

            viewHolder = new ViewDetailsHolder();
            viewHolder.shop_name_single = singleView.findViewById(R.id.shop_name_single);
            viewHolder.shop_location_single = singleView.findViewById(R.id.shop_location_single);

            singleView.setTag(viewHolder);

        } else {
            viewHolder = (ViewDetailsHolder) singleView.getTag();
        }

        if(mdTransactions != null){

            viewHolder.shop_name_single.setText(mdTransactions.name);
            viewHolder.shop_location_single.setText(mdTransactions.location);

        }

        return singleView;

    }

    private class ViewDetailsHolder
    {
        AppCompatTextView shop_name_single, shop_location_single;
    }

}
