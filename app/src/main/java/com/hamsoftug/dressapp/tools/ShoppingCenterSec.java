package com.hamsoftug.dressapp.tools;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.hamsoftug.dressapp.fragments.AddShoppingCenter;
import com.hamsoftug.dressapp.fragments.AvailableShoppingCenter;

/**
 * Created by USER on 6/25/2017.
 */

public class ShoppingCenterSec extends FragmentPagerAdapter {

    public ShoppingCenterSec(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;

        switch (position){
            case 0:
                fragment = new AddShoppingCenter();
                break;

            case 1:
                fragment = new AvailableShoppingCenter();
                break;
            default:
                fragment = new AddShoppingCenter();
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case 0:
                return "Add";
            case 1:
                return "Available";
        }

        return null;
    }
}
