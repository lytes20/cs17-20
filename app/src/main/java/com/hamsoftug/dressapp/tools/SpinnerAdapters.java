package com.hamsoftug.dressapp.tools;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;

import com.hamsoftug.dressapp.R;

import java.util.ArrayList;

/**
 * Created by USER on 6/26/2017.
 */

public class SpinnerAdapters {

    public static class Shop extends BaseAdapter implements SpinnerAdapter{

        private ArrayList<Models.Shop> shops;
        LayoutInflater inflater;

        public Shop(Context context, ArrayList<Models.Shop> shops) {
            this.shops = shops;
            this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return shops.size();
        }

        @Override
        public Object getItem(int i) {
            return shops.get(i);
        }

        @Override
        public long getItemId(int i) {
            return (long)i;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {

            View layout = inflater.inflate(R.layout.single_shop, viewGroup, false);

            AppCompatTextView full_name = layout.findViewById(R.id.shop_name_single);
            AppCompatTextView location = layout.findViewById(R.id.shop_location_single);

            Models.Shop shop = shops.get(position);
            full_name.setText(shop.name);
            location.setText(shop.location);

            return  layout;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {

            View layout = inflater.inflate(R.layout.single_shop, parent, false);

            AppCompatTextView full_name = layout.findViewById(R.id.shop_name_single);
            AppCompatTextView location = layout.findViewById(R.id.shop_location_single);

            Models.Shop shop = shops.get(position);
            full_name.setText(shop.name);
            location.setText(shop.location);

            return  layout;

        }
    }

    public static class Category extends BaseAdapter implements SpinnerAdapter{

        private ArrayList<Models.Category> categories;
        LayoutInflater inflater;

        public Category(Context context, ArrayList<Models.Category> categories) {
            this.categories = categories;
            this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return categories.size();
        }

        @Override
        public Object getItem(int i) {
            return categories.get(i);
        }

        @Override
        public long getItemId(int i) {
            return (long)i;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {

            View layout = inflater.inflate(R.layout.single_shop, viewGroup, false);

            AppCompatTextView full_name = layout.findViewById(R.id.shop_name_single);
            AppCompatTextView location = layout.findViewById(R.id.shop_location_single);
            location.setVisibility(View.GONE);

            Models.Category shop = categories.get(position);
            full_name.setText(shop.name);

            return  layout;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {

            View layout = inflater.inflate(R.layout.single_shop, parent, false);

            AppCompatTextView full_name = layout.findViewById(R.id.shop_name_single);
            AppCompatTextView location = layout.findViewById(R.id.shop_location_single);
            location.setVisibility(View.GONE);

            Models.Category shop = categories.get(position);
            full_name.setText(shop.name);

            return  layout;

        }
    }
}
