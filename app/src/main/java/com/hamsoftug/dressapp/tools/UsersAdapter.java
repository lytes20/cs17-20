package com.hamsoftug.dressapp.tools;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.hamsoftug.dressapp.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.hamsoftug.dressapp.tools.Constants.BASE_URL;
import static com.hamsoftug.dressapp.tools.Constants.USER_TYPE_ADMIN;
import static com.hamsoftug.dressapp.tools.Constants.USER_TYPE_CASUAL;

/**
 * Created by USER on 6/29/2017.
 */

public class UsersAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<Models.User> users;

    public UsersAdapter(Context context, ArrayList<Models.User> users) {
        this.context = context;
        this.users = users;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int i) {
        return users.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewDetailsHolder viewHolder;

        View singleView = view;

        if(singleView==null){

            singleView = inflater.inflate(R.layout.single_user,null,true);
            viewHolder = new ViewDetailsHolder();

            viewHolder.full_names = singleView.findViewById(R.id.single_user_names);
            viewHolder.email = singleView.findViewById(R.id.single_user_email);
            viewHolder.type = singleView.findViewById(R.id.single_user_type);

            singleView.setTag(viewHolder);

        } else {
            viewHolder = (ViewDetailsHolder) singleView.getTag();
        }

        final Models.User user = users.get(i);

        viewHolder.full_names.setText(user.full_names);
        viewHolder.email.setText(user.email_address);
        viewHolder.type.setText(user.type);

        viewHolder.type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view,"Change user type",Snackbar.LENGTH_LONG).setAction("Continue", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Models.User profile = DA.getInstance().getDb().getUsers();

                        if(!profile.number.equals(user.number)){
                            Toast.makeText(context, "Please wait.", Toast.LENGTH_SHORT).show();
                            changeUser(user);
                        } else {
                            Toast.makeText(context, "You can't change your own.", Toast.LENGTH_SHORT).show();
                        }

                    }
                }).show();
            }
        });

        return singleView;
    }

    private class ViewDetailsHolder
    {
        AppCompatTextView full_names, email, type;
    }

    private void changeUser(Models.User user){

        final HashMap<String, String> params = new HashMap<>();
        params.put("change_to",user.type.equals(USER_TYPE_ADMIN)? USER_TYPE_CASUAL: USER_TYPE_ADMIN);
        params.put("user_number", user.number);


        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL+"?todo=change_type", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Toast.makeText(context, response, Toast.LENGTH_LONG).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams(){
                return params;
            }
        };

        DA.getInstance().addToRequestQueue(stringRequest);
    }
}
