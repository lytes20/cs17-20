-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 15, 2017 at 05:11 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dressapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `female_measurements`
--

CREATE TABLE IF NOT EXISTS `female_measurements` (
  `f_id` int(11) NOT NULL,
  `f_o_number` varchar(12) NOT NULL,
  `f_burst` int(4) NOT NULL,
  `f_waist` int(4) NOT NULL,
  `f_hips` int(4) NOT NULL,
  `f_inseam` int(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `female_measurements`
--

INSERT INTO `female_measurements` (`f_id`, `f_o_number`, `f_burst`, `f_waist`, `f_hips`, `f_inseam`) VALUES
(6, '959145', 12, 15, 16, 17),
(7, '254149', 0, 0, 0, 0),
(8, '368248', 45, 25, 32, 50),
(9, '961987', 22, 15, 25, 28),
(10, '288568', 22, 25, 34, 28),
(11, '348177', 25, 35, 41, 23),
(12, '348177', 25, 35, 41, 23),
(13, '348177', 25, 35, 41, 23),
(14, '348177', 25, 35, 41, 23),
(15, '348177', 25, 35, 41, 23);

-- --------------------------------------------------------

--
-- Table structure for table `male_measurements`
--

CREATE TABLE IF NOT EXISTS `male_measurements` (
  `m_id` int(11) NOT NULL,
  `m_o_number` varchar(12) NOT NULL,
  `m_sleeves` int(4) NOT NULL,
  `m_shoulder` int(4) NOT NULL,
  `m_waist` int(4) NOT NULL,
  `m_inseam` int(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `male_measurements`
--

INSERT INTO `male_measurements` (`m_id`, `m_o_number`, `m_sleeves`, `m_shoulder`, `m_waist`, `m_inseam`) VALUES
(4, '120109', 25, 22, 35, 25),
(5, '312035', 0, 0, 0, 0),
(6, '194626', 0, 0, 0, 0),
(7, '635603', 0, 0, 0, 0),
(8, '377705', 0, 0, 0, 0),
(9, '970376', 16, 25, 13, 32),
(10, '851083', 16, 25, 13, 32),
(11, '887069', 16, 32, 25, 41),
(12, '328402', 22, 32, 34, 32),
(13, '715705', 22, 33, 34, 36),
(14, '339228', 0, 0, 0, 0),
(15, '508281', 0, 0, 0, 0),
(16, '330459', 25, 85, 8, 58),
(17, '330459', 25, 85, 8, 58);

-- --------------------------------------------------------

--
-- Table structure for table `t_categories`
--

CREATE TABLE IF NOT EXISTS `t_categories` (
  `c_id` int(11) NOT NULL,
  `c_number` varchar(20) NOT NULL,
  `c_name` varchar(36) NOT NULL,
  `c_user` varchar(12) NOT NULL,
  `c_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_categories`
--

INSERT INTO `t_categories` (`c_id`, `c_number`, `c_name`, `c_user`, `c_date_time`) VALUES
(1, '363749', 'Wedding', '4488', '2017-06-26 19:00:21'),
(2, 'cat56551', 'Conference', '4488', '2017-06-26 19:03:06'),
(3, 'cat60460', 'Introduction', '4488', '2017-06-26 19:04:31'),
(4, 'cat93916', 'Memorial service', '17067977', '2017-06-28 15:22:35'),
(6, 'cat68081', 'Leisure ', '17076039', '2017-07-01 08:37:52');

-- --------------------------------------------------------

--
-- Table structure for table `t_out_fits`
--

CREATE TABLE IF NOT EXISTS `t_out_fits` (
  `o_id` int(11) NOT NULL,
  `o_number` varchar(12) NOT NULL,
  `o_user` varchar(12) NOT NULL,
  `o_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `o_pic` varchar(20) NOT NULL,
  `o_category` varchar(17) NOT NULL,
  `o_shop` varchar(20) NOT NULL,
  `o_desc` text NOT NULL,
  `o_gender` varchar(7) NOT NULL,
  `o_skin` varchar(6) NOT NULL,
  `o_age_range` varchar(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_out_fits`
--

INSERT INTO `t_out_fits` (`o_id`, `o_number`, `o_user`, `o_date_time`, `o_pic`, `o_category`, `o_shop`, `o_desc`, `o_gender`, `o_skin`, `o_age_range`) VALUES
(20, '959145', '17076039', '2017-07-01 08:25:42', '959145.png', '363749', '249121', 'wedding gowns. ', 'Female', 'Light', '23'),
(21, '254149', '17076039', '2017-07-01 08:27:39', '254149.png', '363749', '426393', 'Trebding Gowns ', 'Female', 'Dark', '26'),
(22, '120109', '17076039', '2017-07-01 08:29:27', '120109.png', 'cat56551', '287260', 'Office shirts for men ', 'Male', 'Dark', '28'),
(23, '368248', '17076039', '2017-07-01 08:30:53', '368248.png', 'cat56551', '249121', 'Office wear for ladies ', 'Female', 'Light', '99'),
(24, '312035', '17076039', '2017-07-01 08:32:19', '312035.png', '363749', '426393', 'Wedding siut ', 'Male', 'Dark', '23'),
(25, '194626', '17076039', '2017-07-01 08:34:05', '194626.png', 'cat56551', '249121', 'Males office wear ', 'Male', 'Light', '25'),
(26, '635603', '17076039', '2017-07-01 08:35:36', '635603.png', 'cat60460', '287260', 'Easy shirts ', 'Male', 'Dark', '25'),
(27, '377705', '17076039', '2017-07-01 08:36:33', '377705.png', 'cat60460', '287260', 'kitenge for introduction', 'Male', 'Dark', '25'),
(28, '970376', '17076039', '2017-07-01 08:41:29', '970376.png', 'cat68081', '426393', 'Leisure shirts ', 'Male', 'Light', '25'),
(29, '851083', '17076039', '2017-07-01 08:42:14', '851083.png', 'cat68081', '426393', 'Leisure jackets. ', 'Male', 'Dark', '25'),
(30, '887069', '17076039', '2017-07-01 08:44:15', '887069.png', 'cat68081', '426393', ' Free style couts ', 'Male', 'Light', '25'),
(31, '961987', '17076039', '2017-07-01 08:47:56', '961987.png', 'cat60460', '287260', 'kitenge introduction dress ', 'Female', 'Light', '25'),
(32, '328402', '17076039', '2017-07-01 08:49:38', '328402.png', '363749', '287260', 'Wedding suits for gents ', 'Male', 'Light', '25'),
(33, '288568', '17076039', '2017-07-01 08:50:43', '288568.png', '363749', '249121', 'Growns from Turkey  ', 'Female', 'Light', '25'),
(34, '715705', '17076039', '2017-07-01 08:51:49', '715705.png', '363749', '249121', 'Best wedding siuts ', 'Male', 'Dark', '25'),
(35, '339228', '17076039', '2017-07-01 08:54:34', '339228.png', 'cat68081', '249121', 'leisure ', 'Male', 'Light', '26'),
(36, '508281', '17076039', '2017-07-01 08:55:31', '508281.png', 'cat93916', '249121', 'Feeling sad ', 'Male', 'Light', '32'),
(37, '348177', '17076039', '2017-07-01 08:57:04', '348177.png', 'cat68081', '249121', 'outfits for outing ', 'Female', 'Dark', '32'),
(42, '330459', '4488', '2017-07-01 14:29:46', '330459.png', 'cat93916', '287260', 'heree', 'Male', '', '35');

-- --------------------------------------------------------

--
-- Table structure for table `t_shop`
--

CREATE TABLE IF NOT EXISTS `t_shop` (
  `s_id` int(11) NOT NULL,
  `s_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `s_name` varchar(40) NOT NULL,
  `s_location` varchar(100) NOT NULL,
  `s_contacts` text NOT NULL,
  `s_u_number` varchar(12) NOT NULL,
  `s_number` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_shop`
--

INSERT INTO `t_shop` (`s_id`, `s_date_time`, `s_name`, `s_location`, `s_contacts`, `s_u_number`, `s_number`) VALUES
(1, '2017-06-25 14:04:35', 'Hamsoft shoppers', 'Kawaala kasubi', '0785328136,0755168219', '4488', '249121'),
(3, '2017-06-25 15:38:59', 'Mama beautiful boutique.', 'Jesco building, level 2. Room B3', '0785326,0575365', '4488', '287260'),
(5, '2017-07-01 07:58:43', 'Kim Designers ', 'Kampala road', '075421365', '17076039', '426393');

-- --------------------------------------------------------

--
-- Table structure for table `t_users`
--

CREATE TABLE IF NOT EXISTS `t_users` (
  `u_id` int(11) NOT NULL,
  `u_number` varchar(12) NOT NULL,
  `u_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `u_full_names` text NOT NULL,
  `u_email` varchar(28) NOT NULL,
  `u_password` text NOT NULL,
  `u_gender` varchar(10) NOT NULL,
  `u_type` varchar(10) NOT NULL DEFAULT 'Casual'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_users`
--

INSERT INTO `t_users` (`u_id`, `u_number`, `u_date_time`, `u_full_names`, `u_email`, `u_password`, `u_gender`, `u_type`) VALUES
(1, '4488', '2017-06-25 08:23:01', 'Ham Soft', 'hamsoft2013@gmail.com', '336eb1e90912bf993aaa964df03a8cc2abbbe95b', 'Male', 'Admin'),
(5, '17067977', '2017-06-28 14:58:52', 'amon', 'amonelly2@gmail.com', '20eabe5d64b0e216796e834f52d61fd0b70332fc', 'Female', 'Casual'),
(6, '17076039', '2017-07-01 07:15:58', 'bamuleseyo gideon', 'gideonbamuleseyo@gmail.com', '005687dff60a15b9f4328bbbdb25fbfcd82b3c2c', 'Male', 'Casual'),
(7, '17075070', '2017-07-01 08:08:37', 'kamanda', 'kamjay65@gmail.com', '3552ac2419262405a8b475e1a5092118811903d7', 'Male', 'Casual');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `female_measurements`
--
ALTER TABLE `female_measurements`
  ADD PRIMARY KEY (`f_id`), ADD KEY `f_o_number` (`f_o_number`);

--
-- Indexes for table `male_measurements`
--
ALTER TABLE `male_measurements`
  ADD PRIMARY KEY (`m_id`), ADD KEY `m_o_number` (`m_o_number`);

--
-- Indexes for table `t_categories`
--
ALTER TABLE `t_categories`
  ADD PRIMARY KEY (`c_id`), ADD UNIQUE KEY `c_number` (`c_number`), ADD UNIQUE KEY `c_name` (`c_name`), ADD KEY `c_user` (`c_user`);

--
-- Indexes for table `t_out_fits`
--
ALTER TABLE `t_out_fits`
  ADD PRIMARY KEY (`o_id`), ADD UNIQUE KEY `o_number` (`o_number`), ADD KEY `o_user` (`o_user`), ADD KEY `o_category` (`o_category`), ADD KEY `o_shop` (`o_shop`);

--
-- Indexes for table `t_shop`
--
ALTER TABLE `t_shop`
  ADD PRIMARY KEY (`s_id`), ADD UNIQUE KEY `s_number` (`s_number`), ADD UNIQUE KEY `s_name_2` (`s_name`,`s_location`) COMMENT 'The name and location for a given shop should always be unique and they make a composite key', ADD KEY `s_name` (`s_name`), ADD KEY `s_location` (`s_location`), ADD KEY `s_u_number` (`s_u_number`);

--
-- Indexes for table `t_users`
--
ALTER TABLE `t_users`
  ADD PRIMARY KEY (`u_id`), ADD UNIQUE KEY `u_number` (`u_number`), ADD UNIQUE KEY `u_email` (`u_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `female_measurements`
--
ALTER TABLE `female_measurements`
  MODIFY `f_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `male_measurements`
--
ALTER TABLE `male_measurements`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `t_categories`
--
ALTER TABLE `t_categories`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `t_out_fits`
--
ALTER TABLE `t_out_fits`
  MODIFY `o_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `t_shop`
--
ALTER TABLE `t_shop`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_users`
--
ALTER TABLE `t_users`
  MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `female_measurements`
--
ALTER TABLE `female_measurements`
ADD CONSTRAINT `female_measurements_ibfk_1` FOREIGN KEY (`f_o_number`) REFERENCES `t_out_fits` (`o_number`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `male_measurements`
--
ALTER TABLE `male_measurements`
ADD CONSTRAINT `male_measurements_ibfk_1` FOREIGN KEY (`m_o_number`) REFERENCES `t_out_fits` (`o_number`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_categories`
--
ALTER TABLE `t_categories`
ADD CONSTRAINT `t_categories_ibfk_1` FOREIGN KEY (`c_user`) REFERENCES `t_users` (`u_number`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_out_fits`
--
ALTER TABLE `t_out_fits`
ADD CONSTRAINT `t_out_fits_ibfk_1` FOREIGN KEY (`o_user`) REFERENCES `t_users` (`u_number`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `t_out_fits_ibfk_2` FOREIGN KEY (`o_category`) REFERENCES `t_categories` (`c_number`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `t_out_fits_ibfk_3` FOREIGN KEY (`o_shop`) REFERENCES `t_shop` (`s_number`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_shop`
--
ALTER TABLE `t_shop`
ADD CONSTRAINT `t_shop_ibfk_1` FOREIGN KEY (`s_u_number`) REFERENCES `t_users` (`u_number`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
