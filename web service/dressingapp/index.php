<?php
require_once 'db.php';
class dressApp extends dbclass{
    private $db_properties;
    private $date_time;


    public function __construct() {
        $this->db_properties = $this->conn();
        $this->date_time = date('Y-m-d H:i:s');
    }
        
    public function register(){
        
        $u_number = date('ym').  rand(1000, 9999);
        $full_names = $this->db_properties->real_escape_string($_POST['full_names']);
        $email_address = $this->db_properties->real_escape_string($_POST['email_address']);
        $gender = $this->db_properties->real_escape_string($_POST['gender']);
        $password = $this->db_properties->real_escape_string($_POST['password']);
        
        $password_ = sha1($password);
        
        $res = $this->db_properties->query("insert into t_users set u_date_time='".$this->date_time."', u_number='".$u_number."', u_email='".$email_address."', u_full_names='".$full_names."', u_password='".$password_."', u_gender='".$gender."'");
        
        if($res){
            print "Registration completed successfully";
        } else {
            print "This email is already used, try again with a different email address.\nOr request for password reset if you own this email.";
        }
    }
    
    public function login(){
        $email = $this->db_properties->real_escape_string($_POST['user_email']);
        $password = $this->db_properties->real_escape_string($_POST['password']);
        
        $password_ = sha1($password);
        
        $res = $this->db_properties->query("select * from t_users where u_email='".$email."' and u_password='".$password_."'");
        if($res->num_rows>0){
            
            $response['result'] = array();
            
            $details = $res->fetch_assoc();
            
            $response['result'][] = $details;
            
            //Create json data and push it back to the mobile app.
            print json_encode($response);
            
        } else {
            print "No account associated with the provided email and password, try again.\nOr request for password reset.";
        }        
    }
	
	public function add_shop(){
		
		$contacts = $this->db_properties->real_escape_string($_POST['contacts']);
		$location = $this->db_properties->real_escape_string($_POST['location']);
		$name = $this->db_properties->real_escape_string($_POST['name']);
		$number = $_POST['number'];
		$user_number = $_POST['user_number'];
		
		$res = $this->db_properties->query("insert into t_shop set s_date_time='".$this->date_time."', s_name='".$name."', s_location='".$location."', s_contacts='".$contacts."', s_u_number='".$user_number."', s_number='".$number."'");
		
		if($res){
			print "This shop has been added successfully.\nNo any other shop or boutique will be added with this name and location.";
		} else {
			print "Saving failed.\nShop or boutique with same name and location might have been added.";
		}
		
	}
	
	public function get_shopping_centers(){
		
		$user = $_GET['user'];
		
		$response["result"] = array();
		
		$qry = $this->db_properties->query("select * from t_shop");
		
		while($array=$qry->fetch_assoc()){
			$response['result'][] = $array;
		}
		
		echo json_encode($response);
		
	}
	
	public function add_category(){
		$category = $this->db_properties->real_escape_string($_POST['name']);
		$user = $_POST['user_number'];
		$c_number = "cat".rand(10000,99999); //$_POST['number'];
			
			$res = $this->db_properties->query("insert into t_categories set c_number='".$c_number."', c_user='".$user."', c_name='".$category."', c_date_time='".$this->date_time."'");
			
			if($res){
				print "Category added, you can now select it.";
			} else {
				print "Failed, try again a different category name.";
			}
		
	}
	
	public function get_categories(){
		
		$response["result"] = array();
		
		$res = $this->db_properties->query("select * from t_categories");
		while($array = $res->fetch_assoc()){
                    
                    $c_number = $array['c_number'];
                    $c_name = $array['c_name'];
                    $c_user = $array['c_user'];
                    $c_date_time = $array['c_date_time'];
                    
                    $qry = $this->db_properties->query("select * from t_out_fits where o_category='".$c_number."'");
                    $count = $qry->num_rows;
                                        
			$response['result'][] = array(
                            "c_number" => $c_number,
                            "c_name" => $c_name,
                            "c_user" => $c_user,
                            "c_date_time" => $c_date_time,
                            "item_count" => $count
                        
                    );
		}
		
		echo json_encode($response);
		
	}

	public function add_out_fit(){
		$picture = $_POST['picture'];
		$age_range = $_POST['age_range'];
		$category = $_POST['category'];
		$desc = $this->db_properties->real_escape_string($_POST['description']);
		$gender = $_POST['gender'];
		$number = $_POST['number'];
		$shop = $_POST['shop'];
		$skin = $_POST['skin'];
		$waist = $_POST['waist'];
		$hips = $_POST['hips_shoulders'];
		$inseam = $_POST['inseam'];
		$user = $_POST['user'];
		
		$pic = $number.'.png';
        
        $path = "out_fits/".$pic;
		
		$res_pic = file_put_contents($path,base64_decode($picture));
		
		if($res_pic){
			
		} else {
			print "Failed to write picture at the server.";
			@unlink($path);
			return;
		}
		
		$this->db_properties->query("insert into t_out_fits set o_number='".$number."', o_date_time='".$this->date_time."', o_user='".$user."', o_category='".$category."', o_shop='".$shop."', o_desc='".$desc."', o_gender='".$gender."', o_skin='".$skin."', o_age_range='".$age_range."', o_pic='".$pic."'");
		
		if($gender=="Female"){
		$burst = $_POST['burst_or_sleeves'];
		
		$res = $this->db_properties->query("insert into female_measurements set f_o_number='".$number."', f_burst='".$burst."', f_waist='".$waist."', f_hips='".$hips."', f_inseam='".$inseam."'");
		
		} else {
			
		$shoulders = $_POST['hips_shoulders'];
		$sleeves = $_POST['burst_or_sleeves'];
		
		$res = $this->db_properties->query("insert into male_measurements set m_o_number='".$number."', m_sleeves='".$sleeves."', m_shoulder='".$shoulders."', m_waist='".$waist."', m_inseam='".$inseam."'");
		}
		
		if($res){
			print "Out fit added.";
		} else {
			print "Failed, try again.";
		}
		
	}
	
	public function get_out_fits(){
		
		$response["result"] = array();
		
		$category = $_GET['category'];
		
		$qry = $this->db_properties->query("select * from t_out_fits where o_category='".$category."'");
		
		while($array=$qry->fetch_assoc()){
			
			$o_number = $array['o_number'];
			$o_user = $array['o_user'];
			$o_category = $array['o_category'];
			$o_shop = $array['o_shop'];
			$o_desc = $array['o_desc'];
			$o_gender = $array['o_gender'];
			$o_skin = $array['o_skin'];
			$o_age_range = $array['o_age_range'];
			
			//burst_or_sleeves, waist, hips_shoulders, inseam
			
			if($o_gender=="Female"){
				$qry_2 = $this->db_properties->query("select * from female_measurements where f_o_number='".$o_number."'");
				$results = $qry_2->fetch_array();
				
				$burst_or_sleeves = $results['f_burst'];
				$waist = $results['f_waist'];
				$hips_shoulders = $results['f_hips'];
				$inseam	= $results['f_inseam'];
				
			} else {
				
				$qry_22 = $this->db_properties->query("select * from male_measurements where m_o_number='".$o_number."'");
				$results_ = $qry_22->fetch_array();
				
				$burst_or_sleeves = $results_['m_sleeves'];
				$waist = $results_['m_waist'];
				$hips_shoulders = $results_['m_shoulder'];
				$inseam	= $results_['m_inseam'];
				
			}
			
		$response['result'][] = array(
		"o_number" => $o_number,
		"o_age_range" => $o_age_range,
		"o_category" => $o_category,
		"o_desc" => $o_desc,
		"o_gender" => $o_gender,
		"o_shop" => $o_shop,
		"o_skin" => $o_skin,
		"burst_or_sleeves" => $burst_or_sleeves,
		"waist" => $waist,
		"hips_shoulders" => $hips_shoulders,
		"inseam" =>$inseam
		
		);
		
		
		}
		
		echo json_encode($response);
		
	}
	
	public function get_users(){
		
		$response["result"] = array();
		
		$res =$this->db_properties->query("select * from t_users");
		
		while($array=$res->fetch_assoc()){
			
			$response['result'][] = $array;
			
		}
		
		echo json_encode($response);
	}
	
	public function get_profile(){
		
		$user_id = $_POST['number'];
		$token = $_POST['token'];
		
		$response["result"] = array();
		
		$res =$this->db_properties->query("select * from t_users where u_number='".$user_id."' and u_password='".$token."'");
		
		while($array=$res->fetch_assoc()){
			
			$response['result'][] = $array;
			
		}
		
		echo json_encode($response);
	}
	
	public function change_type(){
		$change_to = $_POST['change_to'];
		$user_number = $_POST['user_number'];
		$res = $this->db_properties->query("update t_users set u_type='".$change_to."' where u_number='".$user_number."'");
		
		if($res){
			print "User type changed to ".$change_to;
		} else {
			print "Failed, try again.";
		}
	}
	
	public function get_shop(){
		
		$shop = $_POST['shop'];
		
		$response["result"] = array();
		
		$res = $this->db_properties->query("select * from t_shop where s_number='".$shop."'");
		
		while($array = $res->fetch_assoc()){
		
		$response['result'][] = $array;
			
		}
		
		echo json_encode($response);
		
	}
    
}

$app = new dressApp();
$todo = filter_input(INPUT_GET, 'todo');

switch ($todo){
    case "register";
        $app->register();
        break;
    
    case "login";
        $app->login();
        break;
		
	case "add_shop";
		$app->add_shop();
		break;
	
	case "get_shopping_list";
		$app->get_shopping_centers();
		break;
		
	case "add_category";
		$app->add_category();
		break;
		
	case "get_categories";
		$app->get_categories();
		break;
		
	case "add_out_fit";
		$app->add_out_fit();
		break;
	
	case "get_out_fits";
		$app->get_out_fits();
		break;
		
	case "get_users";
		$app->get_users();
		break;
		
	case "get_profile";
		$app->get_profile();
		break;
	
	case "change_type";
		$app->change_type();
		break;
		
	case "get_shop";
		$app->get_shop();
		break;
		    
    default:
        print "Unkown request";
        break;
}
